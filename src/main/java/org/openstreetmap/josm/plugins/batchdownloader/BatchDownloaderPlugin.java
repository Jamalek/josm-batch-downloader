package org.openstreetmap.josm.plugins.batchdownloader;

import static org.openstreetmap.josm.tools.I18n.tr;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.MainMenu;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class BatchDownloaderPlugin extends Plugin {

  /**
   * Will be invoked by JOSM to bootstrap the plugin
   *
   * @param info information about the plugin and its local installation
   */
  public BatchDownloaderPlugin(PluginInformation info) {
    super(info);
    boolean added = false;
    MainMenu mainMenu = MainApplication.getMenu();
    for (int i = 0; i < mainMenu.getMenuCount(); i++) {
      JMenu menu = mainMenu.getMenu(i);
      if ("Data".equals(menu.getText())) {
        createAction(menu);
        added = true;
      }
    }
    if (!added) {
      JMenu menu = mainMenu.addMenu(
        "Data",
        tr("Data"),
        -1,
        mainMenu.getDefaultMenuPos(),
        null
      );
      createAction(menu);
    }
  }

  /**
   * Creates the batch-downloader action
   * @param menu
   */
  private void createAction(JMenu menu) {
    MainMenu.add(menu, new JosmAction(
      tr("Batch download"),
      (ImageProvider) null,//new ImageProvider("batch-download.png"),
      tr("Downloads data around a set of features in a batch"),
      Shortcut.registerShortcut("data:batch-download", tr("Data: {0}", tr("Batch download")), KeyEvent.VK_D, Shortcut.CTRL_SHIFT),
      true,
      "aroundselectedfeatures",
      true
    ) {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        new DownloadParamsDialog().showDialog();
      }
    });
  }
}
