package org.openstreetmap.josm.plugins.batchdownloader;

import org.openstreetmap.josm.data.Bounds;
import org.openstreetmap.josm.data.osm.BBox;
import org.openstreetmap.josm.data.osm.Node;

public class BoundsUtils {

  /**
   * Maximum distance of downloaded data from imported points in degrees.
   * 0.001° equals roughly to 100m (on equator)
   */
  private static final double MAX_DISTANCE_DEGREES = 0.001;

  /**
   * Creates a bounding box around a point.
   * @param lat Latitude of center point
   * @param lon Longitude of center point
   * @return Rectandular bounding box
   */
  private static Bounds getPointSurroundingBounds(double lat, double lon) {
    double maxDistanceDegreesLat = MAX_DISTANCE_DEGREES;
    // Meridian convergence correction.
    double maxDistanceDegreesLon = MAX_DISTANCE_DEGREES / Math.cos(Math.toRadians(lat));
    return new Bounds(lat - maxDistanceDegreesLat, lon - maxDistanceDegreesLon,
      lat + maxDistanceDegreesLat, lon + maxDistanceDegreesLon);
  }

  public static Bounds getPointSurroundingBounds (Node point) {
    return getPointSurroundingBounds(point.lat(), point.lon());
  }

  public static Bounds fromBbox(BBox bBox) {
    return new Bounds(bBox.getBottomRightLat(), bBox.getTopLeftLon(), bBox.getTopLeftLat(), bBox.getBottomRightLon());
  }

  public static Bounds add(Bounds bounds, double dist) {
    double distLat = dist*0.000009; //meters to degrees (value / 40 000 000 * 360)
    double distLon = dist*0.000009/Math.cos(Math.toRadians(bounds.getMinLat()));
    return new Bounds(
      bounds.getMinLat()-distLat,
      bounds.getMinLon()-distLon,
      bounds.getMaxLat()+distLat,
      bounds.getMaxLon()+distLon);
  }
}
