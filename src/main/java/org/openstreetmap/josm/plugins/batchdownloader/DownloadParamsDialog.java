package org.openstreetmap.josm.plugins.batchdownloader;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.gui.ExtendedDialog;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.util.WindowGeometry;
import org.openstreetmap.josm.tools.GBC;

/**
 * Class responsible for constructing download dialog and subsequent dialogs and handling user input
 */
public class DownloadParamsDialog extends ExtendedDialog {

  private Container jContentPane;
  private JPanel inputPanel;
  private JTextField distInput;
  private JCheckBox newLayerCheckBox;

  DownloadParamsDialog() {
    super(MainApplication.getMainFrame(),
      tr("Set distance"),
      new String[] {tr("OK"), tr("Cancel")},
      false
    );
    JPanel content = getInputPanel();
    setContent(content);
    try {
      setButtonIcons("ok.png", "cancel.png");
    } catch (Exception e) {
      setButtonIcons("ok.svg", "cancel.svg");
    }
    getJContentPane();
    setDefaultButton(1);
    setupDialog();
    getRootPane().setDefaultButton(defaultButton);
    pack();
    setRememberWindowGeometry(getClass().getName() + ".geometry", WindowGeometry.centerInWindow(MainApplication.getMainFrame(), getPreferredSize()));
    distInput.requestFocusInWindow();
  }

  /**
   * initializes jContentPane
   *
   */
  private void getJContentPane() {
    if (jContentPane == null) {
      jContentPane = this.getContentPane();
      jContentPane.setLayout(new BoxLayout(jContentPane, BoxLayout.Y_AXIS));
      jContentPane.add(getInputPanel(), jContentPane);
    }
  }

  /**
   * initializes inputPanel
   *
   * @return javax.swing.JPanel
   */
  private JPanel getInputPanel() {
    if (inputPanel == null) {

      GridBagConstraints c = new GridBagConstraints();

      JLabel distLabel = new JLabel(tr("Buffer [m]: "));
      distLabel.setFocusable(false); // Needed so that lowest number can have focus immediately
      distLabel.setPreferredSize(new Dimension(111, 16));
      distLabel.setToolTipText(tr("Value added to the minimal bounding box of a feature."));
      distLabel.setPreferredSize(new Dimension(111, 16));

      inputPanel = new JPanel();
      inputPanel.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridwidth = GridBagConstraints.REMAINDER;

      inputPanel.add(distLabel, GBC.std().insets(3, 3, 0, 0));
      inputPanel.add(createDistInput(), GBC.eol().fill(GBC.HORIZONTAL).insets(5, 3, 0, 0));

      newLayerCheckBox = new JCheckBox(tr("Create new layer"), true);
      inputPanel.add(newLayerCheckBox, GBC.eol().insets(3, 3, 0, 0));
    }
    return inputPanel;
  }

  /**
   * Overrides the default actions. Will not close the window when upload trace is clicked
   */
  @Override
  protected void buttonAction(int buttonIndex, final ActionEvent evt) {
    String distText = distInput.getText();
    boolean newLayer = newLayerCheckBox.isSelected();
    if (buttonIndex == 0) try {
      double distance = Double.parseDouble(distText);
      List<OsmPrimitive> features = getFeatures(true);
      if (features.isEmpty()) {
        features = getFeatures(false);
      }
      if (features.isEmpty()) {
        showWaringNoFeatures();
        return;
      }
      if (features.size() > 100) {
        showWaringManyFeatures(features, distance, newLayer);
        return;
      }
      dispose();
      DownloadUtils.downloadAroundFeatures(features, distance, newLayer);
    } catch (NumberFormatException e) {
      ExtendedDialog dialog = new ExtendedDialog(
        MainApplication.getMainFrame(),
        tr("Invalid number format"),
        tr("Cancel"));
      try {
        dialog.setButtonIcons("cancel.png");
      } catch (Exception e2) {
        dialog.setButtonIcons("cancel.svg");
      }
      dialog.setContent("Number input could not be parsed");
      dialog.showDialog();
    } else {
      dispose();
    }
  }

  private void showWaringNoFeatures() {
    ExtendedDialog dialog = new ExtendedDialog(
      MainApplication.getMainFrame(),
      tr("Layer contains no features"),
      tr("Cancel"))
    {
      @Override
      protected void buttonAction(int buttonIndex, final ActionEvent evt) {
        dispose();
      }
    };
    try {
      dialog.setButtonIcons("cancel.png");
    } catch (Exception e) {
      dialog.setButtonIcons("cancel.svg");
    }
    dialog.setContent("Select a layer, that contains features.");
    dialog.showDialog();
  }

  private void showWaringManyFeatures(List<OsmPrimitive> features, double distance, boolean newLayer) {
    ExtendedDialog dialog = new ExtendedDialog(
      MainApplication.getMainFrame(),
      tr("WARNING: Downloading may take a long time!"),
      tr("OK"), tr("Cancel"))
    {
      @Override
      protected void buttonAction(int buttonIndex, final ActionEvent evt) {
        if (buttonIndex == 0) {
          this.dispose();
          DownloadParamsDialog.this.dispose();
          DownloadUtils.downloadAroundFeatures(features, distance, newLayer);
        } else {
          dispose();
        }
      }
    };
    dialog.setContent("You are trying to start "+features.size()+" download tasks, which can take a long time " +
        "(~"+features.size()/100+" minutes, depending feature size, distance and internet connection) and you won't " +
        "be able to use your computer during that time.\nDo you want to continue?");
    try {
      dialog.setButtonIcons("ok.png", "cancel.png");
    } catch (Exception e) {
      dialog.setButtonIcons("ok.svg", "cancel.svg");
    }
    dialog.showDialog();
  }

  private List<OsmPrimitive> getFeatures(boolean selected) {
    List<OsmPrimitive> downloadInput = new ArrayList<>();
    DataSet activeDataSet = MainApplication.getLayerManager().getActiveDataSet();
    //Get all features or all selected features
    Collection<OsmPrimitive> features = activeDataSet.getPrimitives(osmPrimitive -> !selected || osmPrimitive.isSelected());
    for (OsmPrimitive feature : features) {
      if (testNode(feature, features)) {
        downloadInput.add(feature);
      }
    }
    return downloadInput;
  }

  private boolean testNode(OsmPrimitive feature, Collection<OsmPrimitive> dataset) {
    //Add all non point geometries
    if (!(feature instanceof Node))
      return true;
    //Only add points, that are not part of other geometry in resulting dataset
    for (OsmPrimitive referrer : feature.getReferrers()) {
      if (dataset.contains(referrer))
        return false;
    }
    return true;
  }

  /**
   * Initializes distance input
   *
   * @return javax.swing.JTextField
   */
  private JTextField createDistInput() {
    if (distInput == null) {
      distInput = new JTextField();
      distInput.setText("0");
      distInput.addFocusListener(new FocusListener() {
        @Override
        public void focusGained(FocusEvent focusEvent) {
          //Select whatever is inside the input field, when user clicks inside
          distInput.selectAll();
        }
        @Override
        public void focusLost(FocusEvent focusEvent) {
          distInput.select(0, 0);
        }
      });
    }
    return distInput;
  }
}
